package com.kuding.config.exceptionnotice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.boot.autoconfigure.mail.MailSenderAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.MailSender;

import com.kuding.config.annos.ConditionalOnExceptionNotice;
import com.kuding.message.EmailNoticeSendComponent;
import com.kuding.message.INoticeSendComponent;
import com.kuding.pojos.ExceptionNotice;
import com.kuding.properties.notice.EmailNoticeProperty;
import com.kuding.text.NoticeTextResolver;

@Configuration
@AutoConfigureAfter({ MailSenderAutoConfiguration.class })
@ConditionalOnBean({ MailSender.class, MailProperties.class })
@ConditionalOnExceptionNotice
@ConditionalOnProperty(value = "prometheus.email.enabled", havingValue = "email")
public class ExceptionNoticeEmailSendingConfig {

	@Autowired
	private MailSender mailSender;
	@Autowired
	private MailProperties mailProperties;
	@Autowired
	private EmailNoticeProperty emailExceptionNoticeProperty;

	@Bean
	@ConditionalOnMissingBean(parameterizedContainer = INoticeSendComponent.class)
	public INoticeSendComponent<ExceptionNotice> emailNoticeSendComponent() {
		INoticeSendComponent<ExceptionNotice> component = new EmailNoticeSendComponent<ExceptionNotice>(mailSender,
				mailProperties, emailExceptionNoticeProperty, ExceptionNoticeTextResolver());
		return component;
	}

	@Bean
	@ConditionalOnMissingBean
	public NoticeTextResolver<ExceptionNotice> ExceptionNoticeTextResolver() {
		return x -> x.createText();
	}
}
