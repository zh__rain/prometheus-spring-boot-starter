package com.kuding.microservice.interfaces;

import java.util.Set;

import com.kuding.pojos.servicemonitor.MicroServiceReport;
import com.kuding.pojos.servicemonitor.ServiceHealthProblem;
import com.kuding.pojos.servicemonitor.ServiceInstanceLackProblem;

public interface ServiceNoticeRepository {

	void addServiceLackProblem(ServiceInstanceLackProblem serviceInstanceLackProblem);

	void addServiceHealthProblem(ServiceHealthProblem serviceHealthProblem);

	void addLackServices(Set<String> serviceName);

	void addLackServices(String... serviceName);

	MicroServiceReport report();
}
